//Gerenciar as requisições, rotas, urls entre outras funcionalidades.
const express = require("express");
//chamar a função expres
const app = express();
//Criar middleware para receber dados no corpo da requisição
app.use(express.json());
//Testar conexão com o BD
const db = require("./db/models");
//Incluir as controllers
const users = require("./controllers/users");
//Criar as rotas
app.use("/", users);
//Iniciar o servidor
app.listen(8080, () => {
    console.log("Servidor iniciado na porta 8080: http://localhost:8080");
});