//Gerencia as requisições, rotas, urls, etç
const express = require("express");
//Chama a função express
const router = express.Router();
//Incluir o arquivo que possui a conexão com o Banco de Dados
const db = require('./../db/models');
//Listar dados
router.get("/users", async (req, res) => {
    const { page = 1 } = req.query;
    const limit = 10;
    var lastPage = 1;
    const countUser = await db.Users.count();
    if(countUser !== 0){
        lastPage = Math.ceil(countUser / limit);
    }else{
        return res.status(400).json({
            mensagem: "Erro: Nenhum usuário encontrado!"
        });
    }
    //console.log((page * limit) - limit); // 3 * 10 - 10 = 20
    const users = await db.Users.findAll({
        attributes: ['id','name','email'],
        order: [['id','DESC']],
        //Calcular a partir de qual registro deve retornar e o limite de registros
        offset: Number((page * limit) - limit),
        limit: limit
    });

    if(users){
        var pagination = {
            path: "/users",
            page: page,
            prev_page_url: page - 1 >= 1 ? page - 1 : page,
            next_page_url: Number(page) + Number(1) > lastPage ? lastPage : Number(page) + Number(1),
            lastPage: lastPage,
            total: countUser
        }
        return res.json({
            users,
            pagination            
        });
    }else{
        return res.status(400).json({
            mensagem: "Erro: Nenhum usuário encontrado!"
        });
    }
});

//Criar a rota Visualizar
router.get("/users/:id", async (req, res) => {
    const { id } = req.params;
    const user = await db.Users.findOne({
        attributes: ['id','name','email','createdAt','updatedAt'],
        where: {id: id}
    });
    console.log(user);
    return res.json({
        mensagem: "Visualizar!"
    });
});

//Cria a rota cadastrar
router.post("/users", async (req, res) => {
    var dados = req.body;
    //console.log(dados);    
    //Salvar no banco de dados
    await db.Users.create(dados).then((dadosUsuario) => {
        return res.json({
            mensagem: "Usuário cadastrado com sucesso!",
            dadosUsuario
        });
    }).catch(() => {
        return res.json({
            mensagem: "Erro: Usuário não cadastrado!"
        });
    });    
});

module.exports = router;